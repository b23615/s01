package com.zuitt.example;

import java.util.Scanner;

public class S1A1 {
    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);
        System.out.println("First Name: ");
        String userFirstName = userInput.nextLine();

        System.out.println("Last Name: ");
        String userLastName = userInput.nextLine();

        System.out.println("First Subject Grade:");
        double num1 = userInput.nextDouble();

        System.out.println("Second Subject Grade:");
        double num2 = userInput.nextDouble();

        System.out.println("Third Subject Grade:");
        double num3 = userInput.nextDouble();

        System.out.println("Good day, " + userFirstName + " " +userLastName + ".");

        double averageGrade =  (num1 + num2 + num3) / 3;
        System.out.println("Your average grade is: " + Math.floor(averageGrade));
    }
}
